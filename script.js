//факторіал
function factorial(n) {
    if (n === 0 || n === 1) 
    { return 1; } 
    else 
    { return n * factorial(n - 1); }
  }
  console.log("factorial");
  console.log(factorial(1));
  console.log(factorial(2));
  
//відстань між двома точками
function distance(x1, y1, x2, y2) {
    return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
  }
  console.log("distance");
  console.log(distance(0, 0, 1, 1));
  console.log(distance(1, 1, 2, 2));
  
// перевірка: чи є число простим?  
function isPrime(num) {
    const boundary = Math.floor(Math.sqrt(num));
    for (let i = 2; i < boundary; i++) 
    {
      if (num % i === 0) 
      { return false; }
    }
    return num>=2;
  }
  console.log("isPrime");
  console.log(isPrime(0)); 
  console.log(isPrime(1)); 
  
// біномінальний коефіцієнт
function binomialCoefficient(n, k) {
    if(Number.isNan(n) || Number.isNan(k))
    { return NaN; }
    if (k < 0 || k > n) 
    { return 0; }
    if (k === 0 || k === n) 
    { return 1; }
    if (k === 0 || k === n-1) 
    { return n; }
    if (n-k<k) 
    { k=n-k; }
    let res=n;
    for(let i=2; i<=k; i++)
    { res*=(n-i+1)/i; }
    return Math.round(res);
  }
  console.log("binomialCoefficient");
  console.log(binomialCoefficient(8, 3)); 

// відстань Гемінга
function hammingDistance(num1, num2) {
    return ((num1^num2).toString(2).match(1/g)|| "").length;
  }
  console.log("hammingDistance");
  console.log(hammingDistance(22, 22));
  
// найменше загальне кратне кількох чисел
function leastCommonMultiple(...arr) {
    const nok = (...arr)=> {
    const nod =(x,y) => (y ? x :nod(y,x%y));
    const _nok = (x,y) => (x*y)/nod(x,y);
    return [...arr].reduce((a,b)=>_nok(a,b));
  }
  }
  console.log("leastCommonMultiple");
  console.log(leastCommonMultiple(9, 12));
  console.log(leastCommonMultiple(9, 12, 56, 89));   